package com.sda.dp.abstractfactory.ex1.car;

/**
 * Created by amen on 12/2/17.
 */
public abstract class CarFactory {

    public static Car createAudiA4() {
        return new Car("Audi", 60, "A4", 1988);
    }

    public static Car createBMW16() {
        return new Car("BMW", 59, "e36", 1998);
    }
}
