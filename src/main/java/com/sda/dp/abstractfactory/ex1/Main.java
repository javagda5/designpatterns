package com.sda.dp.abstractfactory.ex1;

import com.sda.dp.abstractfactory.ex1.car.Car;
import com.sda.dp.abstractfactory.ex1.car.CarFactory;

/**
 * Created by amen on 12/2/17.
 */
public class Main {
    public static void main(String[] args) {
        Car audiA4 = CarFactory.createAudiA4();
        Car audibmw = CarFactory.createBMW16();
        // can't touch this...
//        Car c = new Car("a", 2, "m", 1992);
    }



    public static void metoda(){
        Car audiA4 = CarFactory.createAudiA4();
    }
}
