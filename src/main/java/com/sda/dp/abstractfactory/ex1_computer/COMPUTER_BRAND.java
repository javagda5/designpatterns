package com.sda.dp.abstractfactory.ex1_computer;

/**
 * Created by amen on 8/8/17.
 */
public enum COMPUTER_BRAND {
    ASUS, SAMSUNG, APPLE, HP
}
