package com.sda.dp.abstractfactory.exaaaaaample.classes;

public class FerrariCar extends Car {
    private FerrariCar(String brand, String model, int horsepower, double engineCapacity) {
        super(brand, model, horsepower, engineCapacity);
    }

    public static Car createFerrari_458() {
        return new FerrariCar("Ferrari", "458", 600, 3.0);
    }
}
