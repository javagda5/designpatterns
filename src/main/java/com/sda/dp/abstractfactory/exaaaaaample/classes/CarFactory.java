package com.sda.dp.abstractfactory.exaaaaaample.classes;

public abstract class CarFactory {

    public static Car createFerrari_458() {
        return new Car("Ferrari", "458", 600, 3.0);
    }

    public static Car createFerrari_458_with_AC() {
        return new Car("Ferrari", "458_with_ac", 600, 3.0);
    }

    public static Car createFerrari_458_with_AC(int horsepower) {
        return new Car("Ferrari", "458_with_ac", horsepower, 3.0);
    }
}
