package com.sda.dp.abstractfactory.exaaaaaample.classes;

public interface ICar {
    public String getModel();
    public String getBrand();
    public double getEngineCapacity();
    public int getHorsepower();
}
