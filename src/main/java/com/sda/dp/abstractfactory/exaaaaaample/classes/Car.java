package com.sda.dp.abstractfactory.exaaaaaample.classes;

class Car implements ICar  {
    private String brand;
    private String model;
    private int horsepower;
    private double engineCapacity;

    protected Car(String brand, String model, int horsepower, double engineCapacity) {
        this.brand = brand;
        this.model = model;
        this.horsepower = horsepower;
        this.engineCapacity = engineCapacity;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getHorsepower() {
        return horsepower;
    }

    public void setHorsepower(int horsepower) {
        this.horsepower = horsepower;
    }

    public double getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }
}
