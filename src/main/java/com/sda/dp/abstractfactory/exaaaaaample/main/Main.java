package com.sda.dp.abstractfactory.exaaaaaample.main;

import com.sda.dp.abstractfactory.ex1.car.Car;
import com.sda.dp.abstractfactory.exaaaaaample.classes.CarFactory;
import com.sda.dp.abstractfactory.exaaaaaample.classes.FerrariCar;
import com.sda.dp.abstractfactory.exaaaaaample.classes.ICar;

public class Main {
    public static void main(String[] args) {
//        Car c = new com.sda.dp.abstractfactory.exaaaaaample.classes.Car("Ferrari", "458", 600, 3.0);
//        CarFactory fabryka = new CarFactory();

        ICar car = CarFactory.createFerrari_458();
        ICar otherCar = CarFactory.createFerrari_458_with_AC();
        ICar carF =  FerrariCar.createFerrari_458();
    }
}
