package com.sda.dp.singleton.ex1.postoffice;

/**
 * Created by amen on 12/2/17.
 */
public class WaitingRoomMachine {
    public Ticket generateTicket(){
//        int ticketId = TicketGenerator.getInstance().getCounter();
        int ticketId = TicketGenerator.INSTANCE.getCounter();

        Ticket ticket = new Ticket("Machine", ticketId);
        return ticket;
    }
}
