package com.sda.dp.singleton.ex1.postoffice;

/**
 * Created by amen on 12/2/17.
 */
public class PostOffice {
    private WaitingRoomMachine machine = new WaitingRoomMachine();
    private Reception reception = new Reception();

    public Ticket generateTicketMachine() {
        System.out.println(machine.generateTicket());
        return machine.generateTicket();
    }

    public void generateTicketReception() {
        System.out.println(reception.generateTicket());
    }

}
