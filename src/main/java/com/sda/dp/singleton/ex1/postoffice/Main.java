package com.sda.dp.singleton.ex1.postoffice;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by amen on 12/2/17.
 */
public class Main {
    public static void main(String[] args) {
        // forma 4
//        TicketGenerator.INSTANCE.metoda();
        // forma 1
//        TicketGenerator.getInstance().getCounter();

        Scanner sc = new Scanner(System.in);

        PostOffice healthDepartment = new PostOffice();

        boolean isWorking = true;
        while (isWorking) {
            String line = sc.nextLine().trim().toLowerCase();

            if (line.startsWith("quit")) {
                break;
            } else if (line.startsWith("machine")) {
                healthDepartment.generateTicketMachine();
            } else if (line.startsWith("reception")) {
                healthDepartment.generateTicketReception();
            }
        }


        List<Ticket> tickets = new LinkedList<>();
        for (int i = 0; i < 1000; i++) {
            tickets.add(healthDepartment.generateTicketMachine());
        }
//
//        String string = "";
//        for (Ticket t : tickets) {
//            string += t.getOrigin() + " ";
//        }
//        System.out.println(string);
        //
        StringBuilder string = new StringBuilder();
        for (Ticket t : tickets) {
            string.append(t.getOrigin()).append(" ");
        }
        System.out.println(string.toString());

    }
}
