package com.sda.dp.observer.example_java;

/**
 * Created by amen on 12/3/17.
 */
public class Main {
    public static void main(String[] args) {
        Restaurant restaurant = new Restaurant();

        restaurant.addObserver(new Employee("A"));
        restaurant.addObserver(new Employee("B"));
        restaurant.addObserver(new Employee("C"));
        restaurant.addObserver(new Employee("D"));
        restaurant.addObserver(new Employee("E"));
        restaurant.addObserver(new Employee("F"));

        restaurant.newOrder();
        restaurant.newOrder("SER");

    }
}
