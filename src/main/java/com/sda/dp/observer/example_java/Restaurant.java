package com.sda.dp.observer.example_java;

import java.util.Observable;

/**
 * Created by amen on 12/3/17.
 */
public class Restaurant extends Observable {
// public class SmsStation extends Observable {

    public void newOrder() {
        setChanged();
        notifyObservers("Komunikat");
    }

    // public void addPhone(String numer){
        // Phone phone = new Phone(numer);
        // addObserver(phone);
    // }

    public void newOrder(String zawartoscZamowienia) {
        Order o = new Order(zawartoscZamowienia);
        // Message message = new Message(numer, tresc);

        setChanged();
        notifyObservers(o);
        // setChanged();
        // notifyObservers(message);
    }

    public void confirmMessage(){
        System.out.println("Received confirmation");
    }
}
