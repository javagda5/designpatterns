package com.sda.dp.threadpool.factorialcalculator;

import java.util.Scanner;

/**
 * Created by amen on 12/9/17.
 */
public class Main {
    public static void main(String[] args) {
        AsynchronousFactorialCalculator calculator = new AsynchronousFactorialCalculator();

        Scanner sc = new Scanner(System.in);

        boolean working = true;
        while (working) {
            int liczba = sc.nextInt();

            calculator.calculateFactorial(liczba);
        }
    }
}
