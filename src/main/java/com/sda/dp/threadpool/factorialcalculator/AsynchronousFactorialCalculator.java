package com.sda.dp.threadpool.factorialcalculator;

import java.util.*;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by amen on 12/9/17.
 */
public class AsynchronousFactorialCalculator {
    private ExecutorService pula = Executors.newFixedThreadPool(5);
    private List<String> results = new ArrayList<>();

    public void calculateFactorial(int number){
        RequestFactorial request = new RequestFactorial(number);
        pula.submit(request);
    }

    // TODO: dokończyć
    public void calculatePower(int number){
        //
    }

}
