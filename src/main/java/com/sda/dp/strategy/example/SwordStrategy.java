package com.sda.dp.strategy.example;

/**
 * Created by amen on 12/9/17.
 */
public class SwordStrategy implements IStrategy {
    @Override
    public void fight() {
        System.out.println("Walczę mieczem!");
    }
}
