package com.sda.dp.strategy.example;

/**
 * Created by amen on 12/9/17.
 */
public class CowardStrategy implements IStrategy {
    @Override
    public void fight() {
        System.out.println("Uciekam!!!!");
        throw new IllegalArgumentException("Nie umim walczyć!");
    }
}
