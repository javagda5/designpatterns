package com.sda.dp.filesearch;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        StringBuilder builder = new StringBuilder();
        builder.append("xyz");
        System.out.println(builder);

//        recurrentSearch("/home/amen/", "singleton");

    }

    public static void recurrentSearch(String path, String name){
        File file = new File(path);

        if(file.isDirectory()){
            File[] listOfFiles = file.listFiles();
            for (File f: listOfFiles) {
                if(f.getName().startsWith(name)){
                    System.out.println(f.getAbsolutePath());
                }
                if(f.isDirectory()) {
                    recurrentSearch(f.getAbsolutePath(), name);
                }
            }
        }
    }
}
