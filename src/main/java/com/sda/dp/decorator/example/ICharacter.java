package com.sda.dp.decorator.example;

/**
 * Created by amen on 12/6/17.
 */
public interface ICharacter {

    int getSpeed();

    int getHealth();

    int getAttackPoints();

    int getDefencePoints();

    //...
}
