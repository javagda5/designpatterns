package com.sda.dp.decorator.pizzeria;

import java.util.*;
/**
 * Created by amen on 12/6/17.
 */
public abstract class AbstractPizza {
    int price;
    List<String> ingr;

    public AbstractPizza(int price, List<String> ingr) {
        this.price = price;
        this.ingr = ingr;
    }
}
