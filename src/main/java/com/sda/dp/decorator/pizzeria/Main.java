package com.sda.dp.decorator.pizzeria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
/**
 * Created by amen on 12/6/17.
 */
public class Main {
    public static void main(String[] args) {
        IPizza pizza = PizzeriaFactory.createPineapplePizza();
        IPizza zBoczkiem = new CustomPizza(pizza, "BOCZEK");


        IPizza nowiutka = new SimplePizza();

        List<String> lista = Arrays.asList("Ciasto", "Ser");
        List<String> kopia = new ArrayList<>();

        Collections.copy(kopia, lista);


    }
}
