package com.sda.dp.decorator.pizzeria;

import java.util.*;
/**
 * Created by amen on 12/6/17.
 */
public interface IPizza {
    int getPrice();
    List<String> getIngredients();
}
