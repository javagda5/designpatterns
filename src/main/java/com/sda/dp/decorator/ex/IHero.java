package com.sda.dp.decorator.ex;

public interface IHero {
    public int getAttackPoints();
    public int getDefendPoints();
}
