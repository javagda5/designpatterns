package com.sda.dp.decorator.ex;

import java.util.Arrays;
import java.util.List;

public class VarArgsExamoke {
    public static void main(String[] args) {

        doSomething(Ingredient.Some, Ingredient.other);// varargs

        doSomething(Arrays.asList(Ingredient.Some, Ingredient.other));// lista
    }

    public static void doSomething(Ingredient... ingredients){

    }

    public static void doSomething(List<Ingredient> ingredients){

    }
}
