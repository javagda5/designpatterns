package com.sda.dp.decorator.ex;

public class HeroOnAHorse implements IHero {
    private int strength;
    private IHero bohater;

    public HeroOnAHorse(IHero bohater, int strength) {
        this.bohater = bohater;
        this.strength = strength;
    }

    public int getAttackPoints() {
        return bohater.getAttackPoints() + strength;
    }

    public int getDefendPoints() {
        return bohater.getDefendPoints() + strength*2;
    }
}
