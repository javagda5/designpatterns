package com.sda.dp.decorator.ex;

public class Hero implements IHero{
    private String name;
    private int health;
    private int points;
    private int attackPoints;
    private int defendPoints;

    public Hero(String name, int health, int points) {
        this.name = name;
        this.health = health;
        this.points = points;
        this.attackPoints = 100;
        this.defendPoints = 100;
    }

    public int getAttackPoints() {
        return attackPoints;
    }

    public void setAttackPoints(int attackPoints) {
        this.attackPoints = attackPoints;
    }

    public int getDefendPoints() {
        return defendPoints;
    }

    public void setDefendPoints(int defendPoints) {
        this.defendPoints = defendPoints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
