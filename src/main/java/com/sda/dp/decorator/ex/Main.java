package com.sda.dp.decorator.ex;

public class Main {
    public static void main(String[] args) {
        Hero bohater = new Hero("Maniek", 100, 100);

        Stables stajnia = new Stables();

        HeroOnAHorse ziomNaKoniu = stajnia.tuneMyHero(bohater);
        HeroOnAHorse ziomNaKoniuKtóryJestNaKoniu = stajnia.tuneMyHero(ziomNaKoniu);
    }
}
