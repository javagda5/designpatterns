package com.sda.dp.decorator.carshop;

/**
 * Created by amen on 12/6/17.
 */
public class CarShop {

    public ICar addExtraCharger(ICar car) {
        return new TunedCar(car, true, false);
    }
}
