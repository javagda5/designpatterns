package com.sda.dp.decorator.carshop;

/**
 * Created by amen on 12/6/17.
 */
public interface ICar {
    double getHorsePower();
    boolean hasCharger();
    double getEngineCapacity();
    double getChargerPressure();
}
