package com.sda.dp.decorator.carshop;

/**
 * Created by amen on 12/6/17.
 */
public class Main {
    public static void main(String[] args) {

        Car car = CarFactory.createAudiA8();

        CarShop sebpol = new CarShop();
        ICar tunedCar = sebpol.addExtraCharger(car);


        System.out.println(car);
        System.out.println(tunedCar);
    }
}
