package com.sda.dp.decorator.carshop;

/**
 * Created by amen on 12/6/17.
 */
public class Car implements ICar {
    private double horsePower;
    private double engineCapacity;
    private boolean charger;
    private double chargerPressure;
    private int seatsNumber;

    public Car(double horsepower, double engineCapacity, boolean hasCharger, double chargerPressure, int seatsNumber) {
        this.horsePower = horsepower;
        this.engineCapacity = engineCapacity;
        this.charger = hasCharger;
        this.chargerPressure = chargerPressure;
        this.seatsNumber = seatsNumber;
    }

    @Override
    public double getHorsePower() {
        return horsePower;
    }

    @Override
    public boolean hasCharger() {
        return charger;
    }

    @Override
    public double getChargerPressure() {
        return chargerPressure;
    }

    @Override
    public double getEngineCapacity() {
        return engineCapacity;
    }

    public void setHorsePower(double horsePower) {
        this.horsePower = horsePower;
    }


    public void setEngineCapacity(double engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public void setCharger(boolean charger) {
        this.charger = charger;
    }

    public void setChargerPressure(double chargerPressure) {
        this.chargerPressure = chargerPressure;
    }

    public int getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(int seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    @Override
    public String toString() {
        return "Car{" +
                "horsePower=" + horsePower +
                ", engineCapacity=" + engineCapacity +
                ", charger=" + charger +
                ", chargerPressure=" + chargerPressure +
                ", seatsNumber=" + seatsNumber +
                '}';
    }
}
