package com.sda.dp.decorator.carshop;

/**
 * Created by amen on 12/6/17.
 */
public class CarFactory {
    public static Car createAudiA8() {
        return new Car(100.0, 2.0, false, 0.0, 2);
    }
}
