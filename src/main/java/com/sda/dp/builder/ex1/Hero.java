package com.sda.dp.builder.ex1;

/**
 * Created by amen on 12/2/17.
 */
public class Hero {
    private int hp = 100, manaPoints, shoeSize, strength, stamina, height;
    private String name, surname, mothersName, fathersName, catsName;
    private double weight;
    private char favouriteLetter;

    private Hero(int hp, int manaPoints, int shoeSize, int strength, int stamina, int height, String name, String surname, String mothersName, String fathersName, String catsName, double weight, char favouriteLetter) {
        this.hp = hp;
        this.manaPoints = manaPoints;
        this.shoeSize = shoeSize;
        this.strength = strength;
        this.stamina = stamina;
        this.height = height;
        this.name = name;
        this.surname = surname;
        this.mothersName = mothersName;
        this.fathersName = fathersName;
        this.catsName = catsName;
        this.weight = weight;
        this.favouriteLetter = favouriteLetter;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "hp=" + hp +
                ", manaPoints=" + manaPoints +
                ", shoeSize=" + shoeSize +
                ", strength=" + strength +
                ", stamina=" + stamina +
                ", height=" + height +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", mothersName='" + mothersName + '\'' +
                ", fathersName='" + fathersName + '\'' +
                ", catsName='" + catsName + '\'' +
                ", weight=" + weight +
                ", favouriteLetter=" + favouriteLetter +
                '}';
    }

    public static class Builder{

        private int hp = 100;
        private int manaPoints = 1000;
        private int shoeSize;
        private int strength;
        private int stamina;
        private int height;
        private String name;
        private String surname;
        private String mothersName;
        private String fathersName;
        private String catsName;
        private double weight;
        private char favouriteLetter;

        public Builder setHp(int hp) {
            this.hp = hp;
            return this;
        }

        public Builder setManaPoints(int manaPoints) {
            this.manaPoints = manaPoints;
            return this;
        }

        public Builder setShoeSize(int shoeSize) {
            this.shoeSize = shoeSize;
            return this;
        }

        public Builder setStrength(int strength) {
            this.strength = strength;
            return this;
        }

        public Builder setStamina(int stamina) {
            this.stamina = stamina;
            return this;
        }

        public Builder setHeight(int height) {
            this.height = height;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        public Builder setMothersName(String mothersName) {
            this.mothersName = mothersName;
            return this;
        }

        public Builder setFathersName(String fathersName) {
            this.fathersName = fathersName;
            return this;
        }

        public Builder setCatsName(String catsName) {
            this.catsName = catsName;
            return this;
        }

        public Builder setWeight(double weight) {
            this.weight = weight;
            return this;
        }

        public Builder setFavouriteLetter(char favouriteLetter) {
            this.favouriteLetter = favouriteLetter;
            return this;
        }

        public Hero createHero() {
            return new Hero(hp, manaPoints, shoeSize, strength, stamina, height, name, surname, mothersName, fathersName, catsName, weight, favouriteLetter);
        }
    }
}
